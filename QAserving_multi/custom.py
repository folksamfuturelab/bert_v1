from bert import QA

model = QA('model_sv')

doc = 'En kung eller konung är en titel för en manlig monark, det vill säga en statschef för en monarkistat[1] som vanligen sitter på livstid och som ofta enligt en fastställd tronföljd har ärvt positionen. För regenter är den kvinnliga motsvarigheten drottning (ibland preciserat som regerande drottning, för att skapa åtskillnad mot drottninggemål). En suverän stat som har en kung eller drottning som statschef kallas kungarike (eller konungarike) och är en monarki. De regerande monarkerna och deras omedelbara familj, som i modernt avseende brukar utföra uppgifter i det offentliga och för staten på monarkernas uppdrag, kallas kungliga eller kungligheter (se även kungahus).'

q = 'Vad är en titel för en manlig monark?'

answer = model.predict(doc,q)

print(answer['answer'])
# 1975
print(answer)
# dict_keys(['answer', 'start', 'end', 'confidence', 'document']))
