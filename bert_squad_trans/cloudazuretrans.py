import sys
import os, requests, uuid, json

#FREE 2M / month key
#subscriptionKey = '0ae5b14901e64044b1cef16c965fe9ac'
#PAID key
subscriptionKey = 'd76adacc55354c8d902fffcff0fed44b'
#FREE marbo

#subscriptionKey = '4be3495167b64f68b03b87f25f96343c'
#PAID marbo

#subscriptionKey = 'c44522f27d4841a7b3b8b5b599c55cdc'

# If you encounter any issues with the base_url or path, make sure
# that you are using the latest endpoint: https://docs.microsoft.com/azure/cognitive-services/translator/reference/v3-0-translate
base_url = 'https://api.cognitive.microsofttranslator.com'
path = '/translate?api-version=3.0'
params = '&to=sv'
constructed_url = base_url + path + params

headers = {
    'Ocp-Apim-Subscription-Key': subscriptionKey,
    'Content-type': 'application/json',
    'X-ClientTraceId': str(uuid.uuid4())
}

def aztrans(text):
    body = [{
        'text' : text
    }]
    request = requests.post(constructed_url, headers=headers, json=body)
    response = request.json()

    return response[0]['translations'][0]['text']



jinp = sys.argv[1]

with open(jinp, 'r', encoding="utf-8") as fi:
    jf = json.loads(fi.read())

dct = dict(jf)

ctr = 0

tctr = 0

try:
    for ttls in dct['data']:
        print(ctr)
        print(ttls['title'])
        tctr += 1
        for ctxs in ttls['paragraphs']:
            txt = ctxs['context']
            #print('\n' + str(ctr) + '\n')
            if not txt.startswith('MAT'):
                #print(txt)
                ctr += len(txt)
                res = 'MAT'+aztrans(txt)
                #print(res)
                print(res[3:100])
                ctxs['context']  = res
            for qa in ctxs['qas']:
                txt = qa['question']
                if not txt.startswith('MAT'):
                    #print(txt)
                    ctr += len(txt)
                    res = 'MAT'+aztrans(txt)
                    #print(res[2:])
                    qa['question'] = res
    
                ansdict  = dict()
    
                for ans in qa['answers']:
                    txt = ans['text']
                    if not txt.startswith('MAT'):
                        #print(txt)
                        if not txt in ansdict:
                            ctr += len(txt)
                            #print('Not translated yet')
                            res = 'MAT'+aztrans(txt)
                            ansdict[txt] = res
                            #print(res[2:])
                        ans['text'] = ansdict[txt]
        if tctr > 327:
            break

except Exception as e:
    print (e)
    jd = json.dumps(dct,ensure_ascii=False)
    with open(jinp[:-5] + '_svenska_azure.json.dump', 'w', encoding='utf-8') as of:
        of.write(jd)
    sys.exit()

    
jd = json.dumps(dct,ensure_ascii=False)
with open(jinp[:-5] + '_svenska_azure.json', 'w',encoding='utf-8') as of:
    of.write(jd)


#translator = Translator()
#res = translator.translate(text='How are you doing today?',src='en',dest='sv')
#print(res.text)


