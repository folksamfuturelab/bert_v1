import sys
import os, requests, uuid, json

def aztrans(text):
    return text[3:]


jinp = sys.argv[1]

with open(jinp, 'r',  encoding="utf-8") as fi:
    jf = json.loads(fi.read())

dct = dict(jf)

ctr = 0

try:
    for ttls in dct['data']:
        print(ttls['title'])
        for ctxs in ttls['paragraphs']:
            txt = ctxs['context']
            if  txt.startswith('MAT'):
                ctr += len(txt)
                res = aztrans(txt)
                ctxs['context']  = res
            for qa in ctxs['qas']:
                txt = qa['question']
                if txt.startswith('MAT'):
                    ctr += len(txt)
                    res = aztrans(txt)
                    print(res[2:])
                    qa['question'] = res
    
    
                for ans in qa['answers']:
                    txt = ans['text']
                    if txt.startswith('MAT'):
                        #print(txt)
                        if True:
                            ctr += len(txt)
                            #print('Not translated yet')
                            res = aztrans(txt)
                            print(res[2:])
                            ans['text'] = res
except:
    jd = json.dumps(dct,ensure_ascii=False)
    with open(jinp[:-5] + '_fixed.json.dump', 'w') as of:
        of.write(jd)
    sys.exit()

    
jd = json.dumps(dct,ensure_ascii=False)
with open(jinp[:-5] + '_fixed.json', 'w',  encoding="utf-8") as of:
    of.write(jd)


#translator = Translator()
#res = translator.translate(text='How are you doing today?',src='en',dest='sv')
#print(res.text)


