import sys
import json


jinp = sys.argv[1]

with open(jinp, 'r') as fi:
    jf = json.loads(fi.read())


dct = dict(jf)


ctr = 0


try:
    for ttls in dct['data']:
        print(ttls['title'])
        ctr_l = 0
        for ctxs in ttls['paragraphs']:
            txt = ctxs['context']
            if not txt.startswith('MAT'):
                ctr_l += len(txt)
                ctr += len(txt)
            for qa in ctxs['qas']:
                txt = qa['question']
                if not txt.startswith('MAT'):
                    ctr_l += len(txt)
                    ctr += len(txt)
                ansdict  = dict()
                for ans in qa['answers']:
                    txt = ans['text']
                    if not txt.startswith('MAT'):
                        ctr_l += len(txt)
                        ctr += len(txt)
                    if not txt in ansdict:
                        #print('Not translated yet')
                        ansdict[txt] = 'DONE'
        print(ctr_l)
    print(ctr)
except:
    sys.exit()

