import sys
import os, requests, uuid, json


jinp = sys.argv[1]
jisv = sys.argv[2]

with open(jinp, 'r') as fi:
    jf = json.loads(fi.read())

with open(jisv, 'r', encoding="utf-8") as fi:
    js = json.loads(fi.read())

dct = dict(jf)
dsv = dict(js)


if True:
    for it in range(5):#range(len(dct['data'])):
        ################################
        # Category level - it title idx#
        ################################
        ttls = dct['data'][it]
        ttls_sv = dsv['data'][it]
        print(ttls['title'] + '\n')
        print(ttls_sv['title'] + '\n')
        #for ic in range(5):#range(len(ttls['paragraphs'])):
        for ic in range(len(ttls['paragraphs'])):
            ################################
            # Context level - ic contex idx#
            ################################
            ctxs = ttls['paragraphs'][ic]
            ctxs_sv = ttls_sv['paragraphs'][ic]
            ctxt = ctxs['context']
            ctxt_sv = ctxs_sv['context']
            print(ctxt[:80])
            print(ctxt_sv[:80])
            #for iq in range(4):#range(len(ctxs['qas'])):
            for iq in range(len(ctxs['qas'])):
                ################################
                # question level - iq quest idx#
                ################################
                qa = ctxs['qas'][iq]
                qa_sv = ctxs_sv['qas'][iq]
                txt = qa['question']
                txt_sv = qa_sv['question']
                print('\t' + txt)
                print('\t' + txt_sv)
                for ia in range(len(qa['answers'])):
                    ans = qa['answers'][ia]
                    ans_sv = qa_sv['answers'][ia]
                    txt = ans['text']
                    txt_sv = ans_sv['text']
                    start = ans['answer_start']
                    ln = len(ctxt)
                    ln_sv = len(ctxt_sv)
                    pos = float(start)/ln

                    est_st = int(pos*0.75*ln_sv)
                    est_end = int(pos*1.25*ln_sv)

                    #print('start ' + str(start) + '\t' + str(est_st) + '\t' + str(est_end) + '\t' + str(ln))
                    print('\t\tEN ' + txt )
                    print('\t\tSV ' + txt_sv )
                    print('\t\t\t' + ctxt[est_st:est_end + len(txt_sv)])
                    print('\t\t\t' + ctxt_sv[est_st:est_end + len(txt_sv)])
                    
#print(res.text)


