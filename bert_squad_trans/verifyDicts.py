import sys
import os, requests, uuid, json


jinp = sys.argv[1]
jisv = sys.argv[2]

with open(jinp, 'r', encoding="utf-8") as fi:
    jf = json.loads(fi.read())

with open(jisv, 'r', encoding="utf-8") as fi:
    js = json.loads(fi.read())

dct = dict(jf)
dsv = dict(js)

ctrw = 0
ctrok = 0


if True:
    #for it in range(5):#range(len(dct['data'])):
    for it in range(len(dct['data'])):
        ################################
        # Category level - it title idx#
        ################################
        ttls = dct['data'][it]
        ttls_sv = dsv['data'][it]
        #print(ttls['title'] + '\n')
        print(ttls_sv['title'] + '\n')
        #for ic in range(5):#range(len(ttls['paragraphs'])):
        for ic in range(len(ttls['paragraphs'])):
            ################################
            # Context level - ic contex idx#
            ################################
            ctxs = ttls['paragraphs'][ic]
            ctxs_sv = ttls_sv['paragraphs'][ic]
            ctxt = ctxs['context']
            ctxt_sv = ctxs_sv['context']
            #print(ctxt[:80])
            #print(ctxt_sv[:80])
            for iq in range(len(ctxs['qas'])):
                ################################
                # question level - iq quest idx#
                ################################
                qa = ctxs['qas'][iq]
                qa_sv = ctxs_sv['qas'][iq]
                txt = qa['question']
                txt_sv = qa_sv['question']
                #print('\t' + txt)
                #print('\t' + txt_sv)
                print('\t' + txt)
                for ia in reversed(range(len(qa['answers']))):
                    ans = qa['answers'][ia]
                    ans_sv = qa_sv['answers'][ia]
                    txt = ans['text']
                    txt_sv = ans_sv['text']
                    start = ans['answer_start']
                    ln = len(ctxt)
                    ln_sv = len(ctxt_sv)
                    pos = float(start)/ln
                    est_st = max(0,int(pos*0.75*ln_sv)-10)
                    est_end = min(int(pos*1.25*ln_sv)+len(txt_sv)+10,ln_sv-1)
                    sse = ctxt[est_st:est_end] 
                    ssv = ctxt_sv[est_st:est_end] 
                    #print('\t' + ssv)
                    #print('\t\t' + txt_sv)
                    #print('\t\t' + txt)

                    if ssv.lower().find(txt_sv.lower()) < 0:
                        #Try English instead
                        #if ssv.lower().find(txt.lower()) < 0:
                            #print('start ' + str(start) + '\t' + str(est_st) + '\t' + str(est_end) + '\t' + str(ln))
                        print('\t\tEN ' + txt )
                        print('\t\tSV ' + txt_sv )
                        print('\t\t\t' + sse)
                        print('\t\t\t' + ssv)
                        ctrw += 1
                        #else:
                        #    ctrok += 1
                    else:
                        ctrok +=1

    print('Wrong ' + str(ctrw))
    print('OK ' + str(ctrok))

        
#print(res.text)


